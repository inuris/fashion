<?php
function get_contact(){
return
<<<EOF
<div class="wrapper">
	<div class="contact_title font_housegothic">
		Contact us
	</div>
	<div class="clear"></div>
	<div class="contact_box">
		<div class="contact_content_title font_housegothic">
			BY PHONE
		</div>
		<div class="contact_content">
			Call us at 1.800.509.0842</br>
			Stylists are available by phone:</br>
			Monday-Friday, 8am-8pm EST</br>
			Saturday, 10am-3pm EST</br>
			Chat with a stylist</br>
		</div>
		<div class="contact_content_title font_housegothic">
			CHAT WITH A STYLIST
		</div>
		<div class="contact_content">
			Our online agents can help with just about anything from advice on fit and accessories to assistance with locating your order or extending a rental period.</br>
			We are sorry, our online agents are not available at this time. You can email our stylists at help@renttherunway.com or use the Contact form to the left.</br>
			Stylists are available by chat:</br>
			Monday-Friday, 9am-7pm EST</br>
			Saturday, 10am-3pm EST</br>
			Follow RTR</br>
		</div>
		<div class="contact_content_title font_housegothic">
			FOLLOW RTR
		</div>
		<div class="contact_content">
			We want to hear from you! Share your favorite RTR stories and photos, get to know the RTR community, and stay up to date with the latest news!
		</div>
		<div class="contact_link">
			<img src="images/facebook.gif"/> &nbsp; <a href="#">Become a Fan on Facebook</a></br>
			<img src="images/twitter.gif"/> &nbsp; <a href="#">Follow us on Twitter</a></br>
			<img src="images/rss.gif"/> &nbsp; <a href="#">Sign up for our RSS feed</a></br>	
		</div>
	</div>
	<div class="contact_form">
		<div class="contact_content_title font_housegothic">
			BY EMAIL
		</div>
		<div class="contact_form_subtitle font_housegothic">
			How can we help you?
		</div>
		<div class="form_label">Regarding:</div>
		<div class="form_radio">
			<ul>
				<li><input type="radio" name="regarding"/>An Existing Order</li>
				<li><input type="radio" name="regarding"/>Styling Advice</li>
				<li><input type="radio" name="regarding"/>General Help</li>
			</ul>
		</div>
		<div class="clear"></div>
		<div class="form_label">Subject:</div>
		<div>
			<input class="form_textbox" type="text"/>
		</div>
		<div class="clear"></div>
		<div class="form_label">Order Number:</div>
		<div>
			<input class="form_textbox" type="text"/>
		</div>
		<div class="clear"></div>
		<div class="form_label">Message:</div>
		<div>
			<textarea class="form_textbox"></textarea>
		</div>
		<div class="clear"></div>
		<div class="form_label">Full name:</div>
		<div>
			<input class="form_textbox" type="text"/>
		</div>
		<div class="clear"></div>
		<div class="form_label">Email:</div>
		<div>
			<input class="form_textbox" type="text"/>
		</div>
		<div class="clear"></div>
		<div class="form_label">Phone number:</div>
		<div>
			<input class="form_textbox" type="text"/>
		</div>
		<div class="clear"></div>
		<input id="contact_submit" class="info_price font_housegothic" value="Submit"/>
		<div class="clear"></div>
		<div class="form_endtext">
			Press inquiries: Please e-mail press@renttherunway.com</br>
			Career inquiries: Please e-mail careers@renttherunway.com
		</div>
	</div>
</div>
EOF;
}
?>


