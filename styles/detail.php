﻿<?php
function get_detail(){
return
<<<EOF
<div class="wrapper">
	<div class="left_bigheart">
		<div class="left_bigheart_text">
		Did you know?
		</div>
	</div>
	<div class="left_bigheart_title">
		<div style="color:#E32384;font-weight:700;margin:15px 0 0 10px;">IF THE DRESSES DON’T FIT, YOU DON’T PAY!</div>
		<div style="color:#660033;font-weight:700;margin:0 0 0 10px;">If none of the dresses in your order fit, we will issue a full merchandise crediy  (less the cost of shipping).</div>
	</div>
	<div class="clear"></div>
	<div class="breadcrumb">Home / Designers / Moschino Cheap And Chic / <span style="color:#000">Yacht Party Dress</span></div>
	<div class="detail_image">
		<div class="detail_bigimage">
			<img id="big_img" style="width:211px;margin:auto;" src=""/>
		</div>
		<div class="detail_thumbnail_box">
			<div class="detail_thumbnail">
				<span></span>
				<img id="thumb_1" onclick="javascript:get_big_img(1);" style="width:49px;margin:auto;" src="images/thumb_1.jpg"/>
			</div>
			<div class="detail_thumbnail">
				<span></span>
				<img id="thumb_2" onclick="javascript:get_big_img(2);" style="width:49px;margin:auto;" src="images/thumb_2.jpg"/>
			</div>
			<div class="detail_thumbnail">
				<span></span>
				<img id="thumb_3" onclick="javascript:get_big_img(3);" style="width:49px;margin:auto;" src="images/thumb_3.jpg"/>
			</div>
		</div>
	</div>
	<div class="detail_info">
		
		<div class="info_title"><a href="#">PARKER</a></div>
		<div class="info_subtitle">Catch Me If You Can Dress</div>
		<div class="clear"></div>
		<div class="info_price_box">
			<div class="info_price font_housegothic">Rental $60</div>
			<div class="info_price_retail">Retail $250</div>
		</div>
		<div class="clear"></div>
		<div class="info_tabs_box">
			<ul id="info_tabs" class="font_housegothic">
				<li id="info_tabs_notes">
					<a href="#info_tabs_notes" onclick="show('tabs_notes')">Stylist Notes</a>
				</li>
				<li id="info_tabs_details">
					<a href="#info_tabs_details" onclick="show('tabs_detail')">Product Details</a>
				</li>
				<li id="info_tabs_size">
					<a href="#info_tabs_size" onclick="show('tabs_size')">Size &amp; Fit</a>
				</li>
			</ul>
			<div class="tabs_content visible" id="tabs_notes">
				<div>
					Flirt in this sexy but sophisticated dress by Parker on your date with that new fling! He'll be smitten by your style. Go with a clutch like the Whiting &amp; Davis' Black Mesh Clutch to complete your date night look.
					
				</div>
				<br>
				<a href="" class="pink">CHAT WITH A FIT SPECIALIST ›</a>
			</div>
			<div class="tabs_content" id="tabs_detail">
				<p>
					Coral silk(100% silk). Surplice neckline with snap closure. Bloussant waist. 35'' from shoulder to hem. Imported.
				</p>
				<br>
				<a href="" class="pink">CHAT WITH A FIT SPECIALIST ›</a>
			</div>
			<div class="tabs_content" id="tabs_size">
				<div>
					Sized XS-L. Model's height is 5'10''.
				</div>
				<br>
				<a href="#" class="pink">CHAT WITH A FIT SPECIALIST ›</a>
			</div>
			<script>
			function show(id){
				$(".tabs_content").hide();
				$("#"+id).show();
				return null;
			}
			</script>
		</div>
		<div class="info_link">
			<a class="info_price font_housegothic" href="">Rent Now</a>	
			<div class="info_share">SHARE</div>	
			&nbsp;
			<a href="#"><img src="images/facebook.gif"/></a>
			<a href="#"><img src="images/twitter.gif"/></a>
		</div>
		<div class="clear"></div>
		<div class="seperator"></div>
		<div class="clear"></div>
		<div class="info_ask">
			<div class="title">Questions about fit?</div>
			CHAT WITH A FIT SPECIALIST
			(M-F 9am-7pm EST, Saturday 10am-3pm EST)
			Call us 1.800.509.0842 (M-F 8am-8pm EST, Saturday 10am-3pm EST)
			Contact us through the Contact Us or email us at help@renttherunway.com				
		</div>
	</div><!--detail_info-->
	<div class="clear"></div>
	<div class="item_related">
		<div class="seperator"></div>
		<div class="title font_housegothic">Complete your look</div>
		<div class="seperator"></div>
		<div class="image_carousel">
			<div id="slider_set">
				<img src="images/bag_1.png"/>
				<img src="images/bag_2.png"/>
				<img src="images/bag_3.png"/>
				<img src="images/bag_1.png"/>
				<img src="images/bag_2.png"/>
				<img src="images/bag_3.png"/>
				<img src="images/bag_1.png"/>
				<img src="images/bag_2.png"/>
				<img src="images/bag_3.png"/>
				<img src="images/bag_1.png"/>
				<img src="images/bag_2.png"/>
				<img src="images/bag_3.png"/>
			</div>
			<div class="clear"></div>
			<a class="prev" id="slider_set_prev" href="#"><span>prev</span></a>
			<a class="next" id="slider_set_next" href="#"><span>next</span></a>
			<script type="text/javascript" language="javascript">
			$("#slider_set").carouFredSel({
				circular:true,
				infinite		:true,
				width: 798,
				prev : "#slider_set_prev",
			    next : "#slider_set_next",
				items:{
					visible		:6,			
					minimum     :6,
					height:105,
					width:131
					},
				scroll:{
					items		:6,
					fx			:"scroll",
					pauseOnHover:true
				},
				auto:{
					play: false
				}
			}
			);
			</script>			
		</div>	
		<div class="clear"></div>
		<div class="title font_housegothic">You may also like</div>
		<div class="seperator"></div>
		<div class="image_carousel">
			<div id="slider_related">
				<img src="images/model_1.png"/>
				<img src="images/model_2.png"/>
				<img src="images/model_3.png"/>
				<img src="images/model_1.png"/>
				<img src="images/model_2.png"/>
				<img src="images/model_3.png"/>
				<img src="images/model_1.png"/>
				<img src="images/model_2.png"/>
				<img src="images/model_3.png"/>
				<img src="images/model_1.png"/>
				<img src="images/model_2.png"/>
				<img src="images/model_3.png"/>
				<img src="images/model_1.png"/>
				<img src="images/model_2.png"/>
				<img src="images/model_3.png"/>
				<img src="images/model_1.png"/>
				<img src="images/model_2.png"/>
				<img src="images/model_3.png"/>
			</div>
			<div class="clear"></div>
			<a class="prev" id="slider_related_prev" href="#"><span>prev</span></a>
			<a class="next" id="slider_related_next" href="#"><span>next</span></a>
			<script type="text/javascript" language="javascript">
			$("#slider_related").carouFredSel({
				circular:true,
				infinite		:true,
				width: 798,
				prev : "#slider_related_prev",
			    next : "#slider_related_next",
				items:{
					visible		:7,			
					minimum     :7,
					height:175,
					width:175
					},
				scroll:{
					items		:7,
					fx			:"scroll",
					pauseOnHover:true
				},
				auto:{
					play: false
				}
			}
			);
			</script>
			
		</div>		
	</div>
</div>
EOF;
}

?>