<?php
function get_categories_designers(){
return
<<<EOF
<div class="left">
	<div class="left_girl"></div>
	<div class="categories_girl">
		Our stylists are on hand to help you find the best fit and pick the perfect accessories. Call us at 1.800.123.456
	</div>
</div>
EOF;
}
function get_designers(){
return
<<<EOF
<div class="right">
	<div class="designers_title">DESIGNERS</div>
	<div class="designers_list"><a href="#">All</a>|<a href="#">Dresses</a>|<a href="#">Accessories</a></div>
	<div class="clear"></div>
	<div class="designers_col">
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					A
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/adam">ADAM</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/albertaferretti">Alberta Ferretti</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/alexisbittar">Alexis Bittar </a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/aliro">Ali Ro</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/aliceolivia">Alice + Olivia</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/alicetemperley">Alice by Temperley</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/alldressedup">alldressedup </a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/angelsanchez">Angel Sanchez</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/annasui">Anna Sui </a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/antonheunis">Anton Heunis</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/avmax">AV Max</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/azaara">Azaara</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					B
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/badgleymischka">Badgley Mischka</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/badgleymischkajewelry">Badgley Mischka Jewelry</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/bcbgmaxazria">BCBGMAXAZRIA </a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/bibhumohapatra">Bibhu Mohapatra</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/blackhalo">Black Halo</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/blugirl">Blugirl</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/brianreyes">Brian Reyes</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					C
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/calvinkleincollection">Calvin Klein Collection</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/calypsostbarth">Calypso St. Barth</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/camillamarc">camilla and marc</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/caraaccessories">Cara Accessories</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/carlosfalchi">Carlos Falchi</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/carlosmiele">Carlos Miele</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/carmenmarcvalvo">Carmen Marc Valvo </a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/catherinemalandrino">Catherine Malandrino</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/ccskye">CC Skye</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/chamakpriyakakkar">Chamak by Priya Kakkar</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/chanluu">Chan Luu</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/chrisbenz">Chris Benz</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/christiancota">Christian Cota</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/christiansiriano">Christian Siriano</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/ciner">Ciner</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/citrinestones">Citrine by the Stones</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/coralialeets">Coralia Leets</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/crislu">Crislu</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/cushnieetochs">Cushnie Et Ochs </a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/cut25">Cut 25</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/cynthiarowley">Cynthia Rowley </a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					D
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/dgdolcegabbana">D&amp;G by Dolce and Gabbana</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/danielvosovic">Daniel Vosovic</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/dannijo">Dannijo</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/davidmeister">David Meister</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/dianevonfurstenberg">Diane von Furstenberg</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/dianevonfurstenberghandbags">Diane von Furstenberg Handbags </a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/doori">Doo.ri</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					E
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/elietahari">Elie Tahari </a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/elizabethcole">Elizabeth Cole</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/ericksonbeamon">Erickson Beamon</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/erinerinfetherston">ERIN by erin fetherston</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/escada">Escada</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					F
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/category/designers/fallon">Fallon</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/felixrey">Felix Rey </a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/fragments">Fragments</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/franktell">Frank Tell </a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<div class="designers_col">
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					G
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/category/designers/gemmaredux">Gemma Redux</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/gerardyosca">Gerard Yosca</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/gryphon">Gryphon</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					H
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/halstonheritage">Halston Heritage</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/haniiy">Hanii Y</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/hautehippie">Haute Hippie</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/helmutlang">Helmut Lang</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/herv%C3%A9l%C3%A9ger">Hervé Léger</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/househarlow1960">House of Harlow 1960</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					I
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/category/designers/isharya">Isharya</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					J
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/category/designers/janissavitt">Janis Savitt</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/jaygodfrey">Jay Godfrey</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/judithleiber">Judith Leiber</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					K
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/category/designers/karaross">Kara Ross</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/katespadenewyork">kate spade new york </a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/katespadenewyorkaccessories">kate spade new york accessories</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/kendrascott">Kendra Scott </a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/kennethjaylane">Kenneth Jay Lane</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					L
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/lailaazhar">Laila Azhar</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/larok">LaROK</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/laurenmerkin">Lauren Merkin</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/leeangel">Lee Angel</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/lelarose">Lela Rose</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/lillypulitzer">Lilly Pulitzer</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/lillypulitzerhandbags">Lilly Pulitzer Handbags</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/lulufrost">Lulu Frost </a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					M
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/mmissoni">M Missoni</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/markjamesbadgleymischka">Mark &amp; James by Badgley Mischka</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/matthewwilliamson">Matthew Williamson</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/miguelases">Miguel Ases</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/millianna">Millianna </a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/milly">Milly</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/miriamhaskell">Miriam Haskell</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/missoni">Missoni</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/moschino">Moschino </a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/moschinocheapchic">Moschino Cheap And Chic</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					N
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/nanettelepore">Nanette Lepore</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/narcisorodriguez">Narciso Rodriguez</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/nicolemiller">Nicole Miller</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/nicolemilleraccessories">Nicole Miller Accessories </a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/nicoleromano">Nicole Romano</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/ninaricci">Nina Ricci</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					O
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/openingceremony">Opening Ceremony </a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					P
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/parker">Parker</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/pencey">Pencey</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/petersom">Peter Som </a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/petersoronen">Peter Soronen</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/philosophydialbertaferretti">Philosophy Di Alberta Ferretti</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/ports1961">Ports 1961</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/prabalgurung">Prabal Gurung</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/preen">Preen</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/proenzaschouler">Proenza Schouler</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<div class="designers_col">
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					R
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/category/designers/rachelleigh0">Rachel Leigh</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/rachelroy">Rachel Roy</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/radenroro">Radenroro</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/rebeccataylor">Rebecca Taylor </a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/richardchai">Richard Chai</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/rjgraziano">RJ Graziano</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/robertrodriguezblacklabel">Robert Rodriguez Black Label</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/robertrodriguezcollection">Robert Rodriguez Collection</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/rodrigootazu">Rodrigo Otazu</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/roksandailincic">Roksanda Ilincic</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					S
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/sachinbabi">Sachin + Babi </a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/seechloe">See by Chloe</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/sequin">Sequin</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/shoshanna">Shoshanna</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/sophietheallet">sophie theallet</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/stylistsurprise">Stylist Surprise </a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/subversive">Subversive</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/susanhanover">Susan Hanover</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					T
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/category/designers/tedrossi">Ted Rossi</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/temperleylondon">Temperley London</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/thakoon">Thakoon </a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/theia">Theia</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/theory">theory</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/threadsocial">Thread Social</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/tibi">Tibi</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/tombinns">Tom Binns</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/tracyreese">Tracy Reese</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/trinaturk">Trina Turk</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/trudellelaker">Trudelle Laker</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/tulestemarket">Tuleste Market</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/twelfthstreetcynthiavincent">Twelfth Street by Cynthia Vincent</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					V
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/venacava">Vena Cava</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/verawang">Vera Wang</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/versusversace">Versus by Versace</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					W
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/category/designers/wgacavintage">WGACA Vintage </a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/category/designers/whitingdavis">Whiting &amp; Davis </a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					Y
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/yigalazrou%C3%ABl">Yigal Azrouël</a>
							</div>
						</li>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/yumikim">Yumi Kim</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="designers_group">
			<ul>
				<li class="designers_key">
					Z
				</li>
				<li>
					<ul>
						<li>
							<div class="designers_name">
								<a href="/designer_detail/zspokezacposen">Z Spoke Zac Posen</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>
EOF;
}
?>