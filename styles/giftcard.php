<?php
function get_giftcard(){
return
<<<EOF
<div class="wrapper">
	<div class="contact_title font_housegothic">
		Gift Card
	</div>
	<div class="clear"></div>
	<div class="giftcard_img"></div>
	<div class="giftcard_info">
		Give the Gift of Glam this holiday season with a Rent the Runway gift card. 
		It's the ultimate holiday treat for your best friend, sister, mother or girl friend. 
		Available in $50 - $1000 increments as classic or e-cards.</br></br>
		Sign in or join now to purchase one today:</br>
		<div style="margin:20px 0 0 40px;" class="info_price font_housegothic">SIGN IN</div>
		<div style="width:200px; margin:20px 0 0 40px;" class="info_price font_housegothic">JOIN NOW FOR FREE</div>
	</div>
</div>
EOF;
}
?>


