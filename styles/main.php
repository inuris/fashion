<?php
function get_header()
{
return
<<<EOF
<div class="header">
	<div class="logo"></div>
	<div class="topmenu">
		<ul>
			<li><a href="?p=giftcard">Gift cards</a></li>
			<li><a href="#">How it works</a></li>
			<li><a href="#">Invite friends, gift 20$, get 20$</a></li>
			<li><a href="#">Chat with a stylish</a></li>
			<li><a href="#">1.800.509.0842</a></li>
		</ul>
	</div>
	<div class="account font_housegothic">
		<ul>
			<li><a href="#">Join now</a></li>
			<li><a href="#">Already a member?</a></li>
		</ul>
	</div>
	<div class="clear"></div>
</div><!--header-->
EOF;
}

function get_footer(){
return
<<<EOF
<div class="clear"></div>
<div class="footer">
	<div class="footer_wrapper">
		<ul class="footer_nav">
			<li><div class="footer_nav_text"><a onmouseover="show_arrow(1)" onmouseout="hide_arrow()" href="?p=about">ABOUT</a><div id="arrow_1" class="footer_arrow"></div></div></li>
			<li><div class="footer_nav_text"><a onmouseover="show_arrow(2)" onmouseout="hide_arrow()" href="?p=contact">CONTACT US</a><div id="arrow_2" class="footer_arrow"></div></div></li>
			<li><div class="footer_nav_text"><a onmouseover="show_arrow(3)" onmouseout="hide_arrow()" href="?p=giftcard">GIFT CARDS</a><div id="arrow_3" class="footer_arrow"></div></div></li>
			<li><div class="footer_nav_text"><a onmouseover="show_arrow(4)" onmouseout="hide_arrow()" href="?p=press">PRESS</a><div id="arrow_4" class="footer_arrow"></div></div></li>
			<li><div class="footer_nav_text"><a onmouseover="show_arrow(5)" onmouseout="hide_arrow()" href="?p=blog">BLOG</a><div id="arrow_5" class="footer_arrow"></div></div></li>
			<li><div class="footer_nav_text"><a onmouseover="show_arrow(6)" onmouseout="hide_arrow()" href="#">FAQS</a><div id="arrow_6" class="footer_arrow"></div></div></li>
			<li><div class="footer_nav_text"><a onmouseover="show_arrow(7)" onmouseout="hide_arrow()" href="#">HOW IT WORKS</a><div id="arrow_7" class="footer_arrow"></div></div></li>
		</ul>
		<div class="clear"></div>	
		<ul class="footer_menu">
			<li><a href="?p=designers">Designers</a></li>
			<li><a href="?p=product">Designer Dress Rentals</a></li>
			<li><a href="#">Accessory Rentals</a></li>
			<li><a href="#">Rent Dresses for Any Occasion</a></li>
		</ul>
		<br>
		<ul  class="footer_menu footer_privacy">
			<li><a href="#">Privacy</a></li>
			<li><a href="#">Rental Agreement</a></li>
			<li><a href="#">Terms &amp; Conditions</a></li>
			<li>&copy;2011 All Rights Reserved</li>
		</ul>
	</div>
	<div class="link">
		<img src="images/facebook.gif"/>
		<img src="images/twitter.gif"/>
		<img src="images/rss.gif"/>
	</div>
</div><!--footer-->
EOF;
}

function get_nav()
{	
return
<<<EOF
<div class="nav">
	<div class="wrapper">
		<ul class="nav_text font_housegothic">
			<li><a href="?p=product">Dresses</a></li>
			<li><a href="?p=product">Accessories</a></li>
			<li><a href="?p=designers">Designers</a></li>
			<li><a href="?p=occasions">Occasions</a></li>
			<li><a href="?p=product">Rirt Insider</a></li>
		</ul>
		<div class="search">
			<input class="search_textbox" type="text" placeholder="Search"/><input class="search_button" type="button" value=""/>
		</div>
		<div class="clear"></div>
	</div><!--wrapper-->
</div><!--nav-->
EOF;
}

function get_banner(){
return
<<<EOF
<div class="banner_box">
	<a href="#"><div class="banner b1"></div></a>
	<a href="#"><div class="banner b2"></div></a>
	<a href="#"><div class="banner b3"></div></a>
	<a href="#"><div class="b4"></div></a>
	<div class="clear"></div>
</div><!--banner_box-->
EOF;
}

function get_categories(){
return
<<<EOF
<div class="left">
	<div class="left_heart"></div>
	<div class="left_pink"></div>
	<div class="categories">
		<ul>
			<li>ALL DRESS</li>
			<ul class="categorie_dress">
				<li>
					<a>BY COLOR</a>
					<ul class="submenu">									
						<li><a>Black dresses</a></li>
						<li><a>Blue dresses</a></li>
						<li><a>Brown dresses</a></li>
						<li><a>Cream dresses</a></li>
						<li><a>Gold dresses</a></li>
						<li><a>Green dresses</a></li>
						<li><a>Grey dresses</a></li>
						<li><a>Orange dresses</a></li>
						<li><a>Pink dresses</a></li>
						<li><a>Purple dresses</a></li>
					</ul>
				</li>
				<li>
					<a>BY LENGTH</a>
					<ul class="submenu">									
						<li><a>100</a></li>
						<li><a>110</a></li>
						<li><a>120</a></li>
					</ul>
				</li>
				<li>
					<a>BY BODY TYPE</a>
					<ul class="submenu">									
						<li><a>Small</a></li>
						<li><a>Medium</a></li>
						<li><a>Large</a></li>
					</ul>
				</li>
				<li>
					<a>BY STYLE</a>
					<ul class="submenu">									
						<li><a>Classic</a></li>
						<li><a>Modern</a></li>
					</ul>
				</li>								
			</ul>
		</ul>
		
	</div>
</div>
EOF;
}

function get_categories_about(){
return
<<<EOF
<div class="left">
	<div class="left_bigheart"></div>
	<div class="categories">
		<ul class="font_housegothic">
		    <li>The Backstory</li>
		    <li>Team</li>
		    <li>Press</li>
		    <li>Testimonials</li>
		    <li>How RTR Works</li>
		    <li>Contact Us</li>
		    <li>Careers</li>
		</ul>		
	</div>
</div>
EOF;
}
?>