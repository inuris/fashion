<?php
	require_once ("styles/main.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Fashion</title>
		<style type="text/css">
			@font-face {
		    font-family: 'HouseGothicHG23Text-Light';
		    src: url('housegothichg23text-light.eot');
		    src: url('housegothichg23text-light.eot?#iefix') format('embedded-opentype'),
		         url('housegothichg23text-light.woff') format('woff'),
		         url('housegothichg23text-light.ttf') format('truetype'),
		         url('housegothichg23text-light.svg#housegothichg23text-light') format('svg');
		    font-weight: normal;
		    font-style: normal;
		}
		</style>
		<script type="text/javascript" src="scripts/jquery.min.js"></script>
		<script type="text/javascript" src="scripts/script.js"></script>
		<script type="text/javascript" src="scripts/jquery.carouFredSel.js"></script>
		<link rel="stylesheet" type="text/css" href="reset.css"/>
		<link rel="stylesheet" type="text/css" href="style.css"/>
	</head>	
	<body>
		<div class="container">
			<?php
				echo get_header();
				echo get_nav();		
				
				// PRODUCT page
				if (isset($_GET['p']))
				{
					$p=$_GET['p'];
					switch ($p)
					{
						case "product":
						{
							require_once ("styles/dress.php");
							echo get_banner();
							echo "<div class=\"wrapper\">";
							echo get_categories();						
							echo get_dress(9);
							echo "</div>";	
							break;							
						}
						case "about":
						{
							require_once ("styles/about.php");
							echo "<div class=\"wrapper_about\">";
							echo get_categories();
							echo get_about();
							echo "</div>";
							break;		
						}
						case "designer":
						{
							require_once ("styles/designers.php");
							echo get_banner();
							echo get_categories_designers();
							echo get_designers();
							break;		
						}
						case "detail":
						{							
							require_once ("styles/detail.php");	
							echo get_banner();
							echo get_detail();
							break;		
						}
						case "contact":
						{
							require_once ("styles/contact.php");
							echo get_contact();
							break;		
						}
						case "giftcard":
						{
							require_once ("styles/giftcard.php");
							echo get_giftcard();
							break;		
						}
						case "press":
						{
							require_once ("styles/press.php");
							echo get_categories_about();
							echo get_press();
							break;		
						}
					}
				}
				echo get_footer();
				?>
		</div><!--container-->
	</body>
</html>
